def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    try:
        d = loadmat(infile)
    except NotImplementedError:
        import h5py
        d = h5py.File(infile)
    return d

if __name__ == "__main__":
    v5  = read_mat('mat_v5.mat')
    v73 = read_mat('mat_v73.mat')
    print('v5 completed')
    print('v73 completed')

